The Tenure Wave: Your interactions with your professors explained in terms of Quantum Mechanics
===

###### George Higgins Hutchinson
###### [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

![my caption](http://phdcomics.com/comics/archive/phd071408s.gif)

[Credit to Jorge Cham for this comic, which inspired me to write this.](http://phdcomics.com/comics/archive/phd071408s.gif)


Some say that professors aren't human, that they're secret reptilian overlords sent among us for enigmatic purposes. Some say they're incomprehensible eldritch beings, and trying to understand them will only lead us mere mortals to insanity.

I, however, posit that professors are just strangely big manifestations of quantum phenomena.

### The Location/Meeting Time Uncertainty Principle

Where is my PI? When are we going to meet next? These are questions that seem like they would have related answers: My PI is going to be with me at the time we're next scheduled to meet. However, I've found that these two observable properties of a professor are not compatible. Similar to the location and momentum of a particle (described by the [Heisenberg Uncertainty Principle](https://en.wikipedia.org/wiki/Uncertainty_principle)), knowing the location of a professor more precisely reduces the precision with which you know your meeting time, so that the product of the uncertainties of both is at a minimum the [reduced Plank constant](https://en.wikipedia.org/wiki/Planck_constant):

$\Delta x \Delta t_m \geq \hbar$

If you know exactly where to find your PI, for instance at a group meeting, everyone else in your lab who needs to ask him something probably also does, so you'd best get in line to get your question in. And maybe it's a fluke, but whenever I have a set meeting time with a professor, the universe seems to conspire against making that time.

This may not be the only uncertainty relation that professors obey, but it's the only one I've been able to observe so far. As a goal for Further Research, I intend to investigate the possibility of a [Generalized Professor Uncertainty Relation](http://galileo.phys.virginia.edu/classes/751.mf1i.fall02/GenUncertPrinciple.htm).

### Tenure-dependent Location Wave Function

So what if we discard any attempt to know meeting times and just work with location? Can we then predict the location of a professor far into the future? Far enough that I'll be able to find him whenever I have a question?

Unfortunately, no.

Professors only exist in a specific location when being explicitly interacted with. At other times, they are only a [probability distribution](https://en.wikipedia.org/wiki/Wave_function#Position-space_wave_functions). There's a 40% chance of finding your PI in his office having a coffee at 2 in the afternoon, maybe. And this probability distribution changes somewhat predictably in time. As an associate professor works towards tenure, the large number of side projects taken on causes the probability distribution of his location to become more spread out.  Weekly group meetings create a periodic clustering of high probability locations.

At any given time, the professor doesn't actually OCCUPY any single one of those locations. However, the effects of his presence are still measurable. Similar to [single-particle interference](http://www.animations.physics.unsw.edu.au/jw/light/youngs-experiment-single-photons.html), the probability of a professor's presence can be observed in phenomena such as when summer interns spontaneously stop slacking off. But it is only when measured explicitly that a professor collapses from a probability distribution to a specific location.

### Surprise lab visits: proof of spontaneous collapse?

Physicists have long speculated about [the specific nature of collapse](https://en.wikipedia.org/wiki/Interpretations_of_quantum_mechanics), when observation causes a quantum system to stop acting like a distribution and start acting like a discrete particle. One somewhat popular interpretation is that [collapse is a spontaneous process](https://en.wikipedia.org/wiki/Ghirardi%E2%80%93Rimini%E2%80%93Weber_theory), that it happens on its own, very rarely, but for large enough things tightly tied together, one collapse pulls every other part along with it. So, you and I, made of trillions of quantum parts that might collapse at any moment, see each other as real objects, with specific locations, because we are continuously collapsing. However, there has been no conclusive proof of spontaneous collapse. Until now.

Last Friday, my PI came to visit me in the lab, completely unannounced.

Suddenly, I had precise knowledge of his location, without trying to observe it. A professor, on his own, collapsed the tenure function and became an object with specific location.

## About me
George is born and raised right here in Santa Barbara, but currently studies EE/CS at UC Berkeley. He's home for the summer working with Dr Jon Klamkin's Integrated Photonics Lab. He likes it a lot, and not **just** because he gets to play with lasers and call it work.